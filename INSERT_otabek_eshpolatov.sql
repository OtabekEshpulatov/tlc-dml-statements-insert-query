DO
$$
    DECLARE
        f_id               INTEGER;
        f_special_features TEXT[];
        f_fulltext         TSVECTOR;
        first_actor        INTEGER;
        second_actor       INTEGER;
        third_actor        INTEGER;
    BEGIN

        f_special_features := ARRAY ['Trailers', 'Commentaries', 'Behind the Scenes'];
        f_fulltext := TO_TSVECTOR('captain america tony stark hulk tanos avengers');

        INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration,
                          rental_rate, length, replacement_cost, rating, special_features, fulltext)
        VALUES ('Avengers: Infinity War', 'Released in year 2018. And its considered...', '2018', 1, 1, 14, 4.99, 149,
                49.99, 'PG', f_special_features, f_fulltext)
        RETURNING film_id
            INTO f_id;

        -- Inserting actors into actor table
        INSERT INTO actor (first_name, last_name)
        VALUES ('Robert', 'Downey')
        RETURNING actor_id INTO first_actor;
        INSERT INTO actor (first_name, last_name)
        VALUES ('Josh', 'Brolin')
        RETURNING actor_id INTO second_actor;
        INSERT INTO actor (first_name, last_name)
        VALUES ('Chris', 'Hemsworth')
        RETURNING actor_id INTO third_actor;

        -- Inserting into film_category
        insert into film_category(film_id, category_id) values (f_id, 1);

        -- Inserting actors into film_actor table
        INSERT INTO film_actor (actor_id, film_id)
        VALUES (first_actor, f_id);
        INSERT INTO film_actor (actor_id, film_id)
        VALUES (second_actor, f_id);
        INSERT INTO film_actor (actor_id, film_id)
        VALUES (third_actor, f_id);

        -- Inserting favorite movie into store's inventory
        INSERT INTO inventory(film_id, store_id)
        VALUES (f_id, 1);
        INSERT INTO inventory(film_id, store_id)
        VALUES ((SELECT film_id FROM film WHERE title ILIKE 'OSCAR GOLD' AND release_year = 2000), 1);

    END;
$$;
